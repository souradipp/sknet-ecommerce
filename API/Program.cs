using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace API
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // var host = CreateHostBuilder(args).Build();
            // using (var scope = host.Services.CreateScope())
            // {
            //     var serviceProvider = scope.ServiceProvider;
            //     var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            //     try
            //     {
            //         var context = serviceProvider.GetRequiredService<StoreContext>();
            //         await context.Database.MigrateAsync();
            //         await StoreContextSeed.SeedAsync(context, loggerFactory);
            //     }
            //     catch (System.Exception ex)
            //     {
            //         var logger = loggerFactory.CreateLogger<Program>();
            //         logger.LogError(ex, "Error occured while migration");
            //     }
            // }
            // host.Run();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
