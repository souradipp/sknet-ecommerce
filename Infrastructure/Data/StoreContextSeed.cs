using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Core.Entites;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Data
{
    public class StoreContextSeed
    {
        public static async Task SeedAsync(StoreContext context, ILoggerFactory loggerFactory)
        {
            try
            {
                if (!context.ProductBands.Any())
                {
                    var brandsData = File.ReadAllText("../Infrastructure/Data/Seeder/brands.json");
                    var brands = JsonSerializer.Deserialize<List<ProductBrand>>(brandsData);

                    foreach (var item in brands)
                    {
                        context.ProductBands.Add(item);
                    }

                    await context.SaveChangesAsync();
                }

                if (!context.ProductTypes.Any())
                {
                    var brandsData = File.ReadAllText("../Infrastructure/Data/Seeder/types.json");
                    var brands = JsonSerializer.Deserialize<List<ProductType>>(brandsData);

                    foreach (var item in brands)
                    {
                        context.ProductTypes.Add(item);
                    }

                    await context.SaveChangesAsync();
                }

                if (!context.Products.Any())
                {
                    var brandsData = File.ReadAllText("../Infrastructure/Data/Seeder/products.json");
                    var brands = JsonSerializer.Deserialize<List<Product>>(brandsData);

                    foreach (var item in brands)
                    {
                        context.Products.Add(item);
                    }

                    await context.SaveChangesAsync();
                }
            }
            catch (System.Exception ex)
            {

                var logger = loggerFactory.CreateLogger<StoreContextSeed>();
                logger.LogError(ex, "Error wile seeding data");
            }
        }
    }
}